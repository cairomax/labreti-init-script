H=`hostname`

echo "hostname: $H"

cat > /etc/network/interfaces <<END_INTERFACES

$(case $H in
  (router*) echo auto lo eth0 eth1 ;;
  (*) echo auto lo eth0 ;;
  esac
)

iface lo inet loopback

iface eth0 inet static
  address $(case $H in
      (router1) echo 10.0.1.100 ;;
      (router2) echo 10.0.2.100 ;;
      (host01) echo 10.0.1.1 ;;
      (host02) echo 10.0.1.2 ;;
      (host03) echo 10.0.2.3 ;;
      (host04) echo 10.0.3.4 ;;
      esac
    )
  netmask 255.255.255.0
  $(case $H in
      (host01|host02) echo up route add default gw 10.0.1.100 ;;
      (host03) echo up route add default gw 10.0.2.100 ;;
      esac
  )
  
$(case $H in
(router*) cat <<END_ETH
iface eth1 inet static
  address $(case $H in
      (router1) echo 10.0.3.1 ;;
      (router2) echo 10.0.3.2 ;;
      esac
    )
  netmask 255.255.255.0
END_ETH
;;
esac
)

END_INTERFACES

case $H in
  (router*|host04)
    T=`mktemp`
    cat /etc/zebra/daemons | sed 's/ripd=no/ripd=yes/' > $T && cat $T > /etc/zebra/daemons
    ;;
esac

case $H in
  (router*|host04)
    T=`mktemp`
    cat /etc/zebra/ripd.conf | while read line ; do
      case $line in
        (router*rip)
        echo $line
        echo network eth0
        if [ "$H" != "host04" ] ; then
            echo network eth1
        fi
        ;;
        (network*) ;;
        (*)
        echo $line ;;
      esac
    done  > $T && cat $T > /etc/zebra/ripd.conf
    ;;
esac

update-rc.d zebra defaults

invoke-rc.d networking restart
invoke-rc.d zebra restart

case $H in
  (router|host04)
    T=`mktemp`
    cat /etc/bind/named.conf.options | while read line ; do
        case $line in
          (options*)
            echo $line
            echo 'allow-query { 10.0.0.0/16; 127.0.0.1; } ; ';
            ;;
          (allow-query*)
            ;;
          (*)
            echo $line ;;
        esac
    done > $T && cat $T > /etc/bind/named.conf.options
esac

case $H in
  (router*)
  
  cat > /etc/bind/db.root <<END_DB_ROOT
. IN NS dns.mio.localdomain.
dns.mio.localdomain. IN A 10.0.3.4
END_DB_ROOT
  ;;
  (host04)
  
  cat > /etc/bind/db.root <<END_DB_ROOT
END_DB_ROOT
  ;;
esac

case $H in
  (router1)
  cat > /etc/bind/db.localdomain.mio.serv <<'END_DB'
$ORIGIN serv.mio.localdomain. ; la zona definita
$TTL 1d ; il TTL di default
@ IN SOA ( ; Start of Authority
dns.serv.mio.localdomain. ; authoritative master
root.mio.localdomain. ; indirizzo email
2011091200 ; n. seriale
1d ; refresh (...il file)
1h ; retry
2d ; expire
1h ; ttl esportato
)
@ IN NS dns ; il server della zona
dns IN A 10.0.3.1
ftp IN A 10.0.1.2
END_DB
  ;;

  (router2)
  cat > /etc/bind/db.localdomain.mio.lab <<'END_DB'
$ORIGIN lab.mio.localdomain. ; la zona definita
$TTL 1d ; il TTL di default
@ IN SOA ( ; Start of Authority
dns.lab.mio.localdomain. ; authoritative master
root.mio.localdomain. ; indirizzo email
2011091200 ; n. seriale
1d ; refresh (...il file)
1h ; retry
2d ; expire
1h ; ttl esportato
)
@ IN NS dns ; il server della zona
dns IN A 10.0.3.2
host2 IN A 10.0.2.3
host1 IN A 10.0.1.1
END_DB
  ;;

  (host04)
  cat > /etc/bind/db.localdomain.mio <<'END_DB'
$ORIGIN mio.localdomain. ; la zona definita
$TTL 1d ; il TTL di default
@ IN SOA ( ; Start of Authority
dns.mio.localdomain. ; authoritative master
root.mio.localdomain. ; indirizzo email
2011091200 ; n. seriale
1d ; refresh (...il file)
1h ; retry
2d ; expire
1h ; ttl esportato
)
@ IN NS dns ; il server della zona
dns IN A 10.0.3.4
www IN A 10.0.3.4
serv IN NS dns.serv.mio.localdomain.
dns.serv IN A 10.0.3.1
lab IN NS dns.lab.mio.localdomain.
dns.lab IN A 10.0.3.2
END_DB
  ;;
esac

case $H in
  (router1)
  cat > /etc/bind/named.conf.local <<'END_NAMED_CONF'
zone "serv.mio.localdomain" {
type master;
file "/etc/bind/db.localdomain.mio.serv";
};
END_NAMED_CONF
  ;;
  (router2)
  cat > /etc/bind/named.conf.local <<'END_NAMED_CONF'
zone "lab.mio.localdomain" {
type master;
file "/etc/bind/db.localdomain.mio.lab";
};
END_NAMED_CONF
  ;;
  (host04)
  cat > /etc/bind/named.conf.local <<'END_NAMED_CONF'
zone "mio.localdomain" {
type master;
file "/etc/bind/db.localdomain.mio";
};
END_NAMED_CONF
  ;;
esac

update-rc.d bind defaults
invoke-rc.d bind restart

case $H in
  (router1|host02)
  cat > /etc/resolv.conf <<END_RESOLV_CONF
nameserver 10.0.3.1
search serv.mio.localdomain
END_RESOLV_CONF
  ;;
  (router2|host01|host03)
  cat > /etc/resolv.conf <<END_RESOLV_CONF
nameserver 10.0.3.2
search lab.mio.localdomain
END_RESOLV_CONF
  ;;
  (host04)
  cat > /etc/resolv.conf <<END_RESOLV_CONF
nameserver 10.0.3.4
search mio.localdomain
END_RESOLV_CONF
  ;;
esac

# TESTS

IPS="$(for h in 1.1 1.2 1.100 2.3 2.100 3.1 3.2 3.4 ; do echo -n "10.0.$h " ; done)"
NAMES="www dns dns.serv ftp.serv dns.lab host1.lab host2.lab"
FQNAMES="$(for h in $NAMES; do echo -n "$h.mio.localdomain " ; done)"

for h in $IPS ; do traceroute $h ; done
for h in $FQNAMES ; do dig $h ; done

NH=$(
  case $H in
    (host01) echo host1 ;;
    (host02) echo ftp ;;
    (host03) echo host2 ;;
    (host04|router1|router2) echo dns ;;
  esac
)

echo "Run the following line to change the hostname (required for full functionality, but will break this script)"
echo
echo "hostname $NH ; echo $NH > /etc/hostname"

